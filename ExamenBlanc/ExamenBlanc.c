#include<stdio.h>
#include<stdlib.h>


int slen(char *s) {
    int len = 0;
    while (*s != '\0') {
        len++;
        s++;
    }
    return len;
}

int is_upper(char *s) {
    while (*s != '\0') {
        if (*s >= 'a' && *s <= 'z') {
            return 0;
        }
        s++;
    }
    return -1;
}

int is_lower(char *s) {
    while (*s != '\0') {
        if (*s >= 'A' && *s <= 'Z') {
            return 0;
        }
        s++;
    }
    return -1;
}

void supper(char *s) {
    while (*s != '\0') {
        if (*s >= 'a' && *s <= 'z') {
            *s = *s - ('a' - 'A');
        }
        s++;
    }
}

void slower(char *s) {
    while (*s != '\0') {
        if (*s >= 'A' && *s <= 'Z') {
            *s = *s + ('a' - 'A');
        }
        s++;
    }
}


char *scopy(char *s) {
    char *copy = NULL;
    int len = 0;
    while (s[len] != '\0') {
        len++;
    }
    copy = (char*) malloc((len + 1) * sizeof(char));
    if (copy == NULL) {
        return NULL;
    }
    for (int i = 0; i < len; i++) {
        copy[i] = s[i];
    }
    copy[len] = '\0';
    return copy;
}

int sequal(char *s1, char *s2) {
    while (*s1 == *s2) {
        if (*s1 == '\0') {
            return -1;
        }
        s1++;
        s2++;
    }
    return 0;
}

int main(){

    char s;
    printf("ecrit une phrase\n");
    scanf("%c", &s);
}
