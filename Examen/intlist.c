#include <stdio.h>
#include <stdlib.h>

typedef struct node node;
struct node {
    int val;
    node *next;
    node *prev;
};

node *create_node(int val) {
    node *p;
    p = malloc(sizeof(node));
    p->val = val;
    p->next = NULL;
    p->prev = NULL;
    return p;
}

node *append_val(node *head, int val) {
    node *newnode, *walk;

    newnode = create_node(val);

    if (head == NULL) { //une liste vide
        head = newnode;
    } else { //on parcourt la liste jusqu'à la fin
        walk = head;
        while (walk->next != NULL) { //on va jusqu'au dernier nœud
            walk = walk->next;
        }
        walk->next = newnode; //on ajoute le nouvel élément
        newnode->prev = walk; //on définit son précédent
    }
    return head;
}

void print_list(node *head) {
    node *walk;

    walk = head;
    while (walk != NULL) {
        printf("%d ", walk->val);
        walk = walk->next;
    }
    printf("\n");
}

void forth_and_back(node *head) {//on parcourt la liste de gauche à droite
    
    printf("Liste de gauche à droite : ");
    node *current = head;
    while (current != NULL) {
        printf("%d ", current->val);
        current = current->next;
    }
    printf("\n");

    printf("Liste de droite à gauche : ");//on parcourt la liste de droite à gauche
    current = head;
    while (current->next != NULL) { //on va jusqu'au dernier nœud
        current = current->next;
    }
    while (current != NULL) {
        printf("%d ", current->val);
        current = current->prev;
    }
    printf("\n");
}

int main() {
    node *head = NULL;

    head = append_val(head, 1);//on construit la liste
    head = append_val(head, 2);
    head = append_val(head, 3);
    head = append_val(head, 4);

    forth_and_back(head);// on affiche la liste
    
    return 0;
}

