#include <stdio.h>
#include <stdlib.h>
#include <math.h>//il est obligatoire pour norm_vector

void add_vectors(int n, double *t1, double *t2, long double *t3) { //T3[] devient long double et plus double
    for (int i = 0; i < n; i++) {
        double diff = t1[i] - t2[i];
        t3[i] = diff * diff;
    }
}

double norm_vector(int n, double *t) {
    double sum = 0.0;
    for (int i = 0; i < n; i++) {
        sum += t[i] * t[i];
    }
    return sqrt(sum);//on fait le carre de la somme
}

int main() {
    double T1[] = { 3.14, -1.0, 2.3, 0, 7.1 };
	double T2[] = { 2.71,  2.5,  -1, 3, -7  };
	long double T3[5];//j'ai ete obliger de mettre un long double pour que add_vectors() fonction
	int n = 5;//on definit la taille des tableaux
 

    printf("T1 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2lf ", T1[i]);//le .2 permet de limiter à 2 chiffres apres la virgule
	}
	printf("\nT2 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2lf ", T2[i]);
	}
	printf("\n");

    add_vectors(n, T1, T2, T3);

    for (int i = 0; i < n; i++) {
        printf("T3 : %.2Lf ", T3[i]);
    }
    printf("\n");

    double norm = norm_vector(n, T1);
    printf("Norm of the vector T1: %.2f\n", norm);

	
	exit(EXIT_SUCCESS);

}

