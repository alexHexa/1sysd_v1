#include <stdio.h>

int count_words_better(char *tab) { //la fonction count_word prenait deja en compte les espaces cependant si il y a trop d'espace, la premiere version ne compte pas bien les mots
    int word_count = 0;
    int i = 0;

    while (tab[i] != '\0') {
     
        while (tab[i] == ' ') {//on ignore les espaces au début de la chaîne
            i++;
        }

        if (tab[i] != '\0') {//on compter le mot suivant s'il y en a un
            word_count++;

            while (tab[i] != ' ' && tab[i] != '\0') {//on ignore les caractères du mot
                i++;
            }
        }
    }

    return word_count;
}

int count_char(char *tab, char c) {
    int count = 0;
    for(int i = 0; tab[i] != '\0'; i++) {
        if(tab[i] == c) {
            count++;
        }
    }
    return count;
}

int count_words(char *tab) {
    int count = 0;
    int i = 0;

    
    while (tab[i] == ' ') {//on passe les espaces au début de la chaîne
        i++;
    }

   
    while (tab[i] != '\0') {//on compte les mots
        if (tab[i] == ' ') {
            
            while (tab[i] == ' '){//on passe au suivant quand il y a un espace
                i++;
            }
            
            if (tab[i] == '\0') {//si on arrive a la fin de la chaîne apres des espaces, on sort de la boucle
                break;
            }
            count++;
        }
        i++;
    }
    
    if (tab[i-1] != ' ') {//on compte tous les mots
        count++;
    }

    return count;
}


int main() {
    char tab[50];
    char c;
    
    printf("Entrez une chaine de caractères (maximum 49 caractères) : ");
    scanf("%49[^\n]", tab);//fgets(tab, 50, stdin); -- cela permet de limiter a 49 caracteres 
    getchar(); //consomme le caractère de retour à la ligne laisse par scanf

    printf("Entrez un caractère à chercher : ");
    scanf("%c", &c);

    printf("Vous avez saisi la chaîne suivante : %s\n", tab);

    int char_count = count_char(tab, c);
    
    int word_count = count_words(tab);
    
    int better_word_count = count_words_better(tab);
    
    printf("Le caractère '%c' apparaît %d fois dans la chaîne '%s'.\n", c, char_count, tab);
    printf("Il y a %d mots dans la chaîne '%s'\n", word_count, tab);    
    printf("Il y a %d mots dans la chaîne '%s'\n", better_word_count, tab);

    return 0;
}

