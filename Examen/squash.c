#include <stdio.h>

void small_to_zero(int *tab, int n, int val) {
    for (int i = 0; i < n; i++) {
        if (tab[i] <= val) {
            tab[i] = 0;
        }
    }
}

int main() {
    int tab[] = { 1, 3, -1, 2, 7, 5, 12, 4, 4, 3, 0 };
    int n = (sizeof(tab) / sizeof(tab[0]));//la longueur s'adapte en fonction du tableau
    int val = 3; //ici on change le nbr de caractere que l'on veut modifier dans le tableau

    printf("Voici le tableau avant la modification : ");//création d'un avant-après
    for (int i = 0; i < n; i++) {
        printf("%d ", tab[i]);
    }
    printf("\n");//necessaire pour une meilleur visibilité

    small_to_zero(tab, n, val);

    printf("Voici le tableau après la modification : ");
    for (int i = 0; i < n; i++) {
        printf("%d ", tab[i]);
    }
    printf("\n");

    return 0;
}


