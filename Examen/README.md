1.Quelles sont les étapes nécessaires pour supprimer un élément d'une liste chaînée en connaissant son adresse (un pointeur vers le nœud concerné) ?

    On doit d'abord vérifier si le nœud que l'on souhaite supprimer est le premier nœud de la liste. Si tel est le cas, on met à jour l'en-tête de la liste pour pointer vers le nœud suivant. Sinon, la liste est parcourue jusqu'au nœud précédant le nœud à supprimer. On met ensuite à jour le champ suivant du nœud précédent afin qu'il pointe vers le nœud à côté de celui qu'on veut supprimer. Pour finir, on utilise la fonction free() pour libérer la mémoire allouée au nœud à supprimer.


2.Deux fonctions réalisent cette opération dans le fichier linus.c.
Quelles sont les différences notables entre ces deux fonctions ?

    Dans le programme linus.c, on constaste l'apparition des fonctions 'remove_list_entry_good_taste()' et 'remove_list_entry_bad_taste()'.


3.Pourquoi la seconde a besoin de renvoyer une valeur et l'autre non ?

    La seconde programme utilise des fonctions supplémentaires qui attendent tous une valeur. Tandis que le premier programme n'utilise pas de fonctions qui attendent le retour d'une valeur.


4.Quel est le rôle d'un pointeur vers un pointeur ici ?

    Ici, le rôle d'un pointeur de pointeur est d'allouer dynamiquement de la mémoire.


5.Pourquoi la seconde reçoit un pointeur vers un pointeur comme argument ?

    En utilisant un pointeur de pointeur (nœud **phead), on peut directement modifier l'adresse du pointeur de tête de la liste passée en paramètre, au lieu de renvoyer un nouveau pointeur de tête comme la fonction append_val().


6.En quelques mots, laquelle est la plus élégante selon vous ?

    Pour moi, le premier programme est une version simplifier du second. Cependant, le second apport de la précision au code. Je trouve que le second programme est la version la plus élégante.
