#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>



int main() {
    int n;
    int reponse;

    printf("tu dois deviner le chiffre !\n");

    srand(time(NULL));
    n = rand() % 10;

    bool condition = true;

    do {
        scanf("%d", &reponse);


        if (reponse == n)
        {
            printf("Bravo, tu as trouve le chiffre !\n");
            condition = false;
        }
        else
        {
            if (reponse > n)
            {
                printf("Le chiffre cherche est plus petit\n");
            }
            else if (reponse < n)
            {
                printf("Le chiffre cherche est plus grand\n");
            }
            else
            {
                printf("Tu dois écrire un chiffre pour trouver la solution\n");
            }
        }
    } while (condition == true);

}
