#include <stdio.h>
#include <string.h>

int main() {
    char word[100];
    int i, j, len;
    int is_palindrome = 1;  
    printf("Entrez un mot: ");
    scanf("%s", word);
    len = strlen(word);
    
    for (i = 0, j = len - 1; i < j; i++, j--) {
        if (word[i] != word[j]) {
            is_palindrome = 0;  
            break;
        }
    }
    if (is_palindrome) {
        printf("%s est un palindrome.\n", word);
    } else {
        printf("%s n'est pas un palindrome.\n", word);
    }
    return 0;
}
