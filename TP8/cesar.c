#include <stdio.h>
#include <string.h>

// Cette fonction chiffre un caractère selon la clé donnée.
char cipher(char ch, int key) {
    if (ch >= 'a' && ch <= 'z') {
        ch = (ch + key - 'a') % 26 + 'a';
    } else if (ch >= 'A' && ch <= 'Z') {
        ch = (ch + key - 'A') % 26 + 'A';
    }
    return ch;
}

// Cette fonction chiffre une chaîne de caractères selon la clé donnée.
void encrypt(char message[], int key) {
    int i;
    for(i = 0; i < strlen(message); i++) {
        message[i] = cipher(message[i], key);
    }
}

// Fonction principale
int main() {
    char message[100];
    int key;

    printf("Entrez le message à chiffrer: ");
    fgets(message, 100, stdin);

    printf("Entrez la clé de chiffrement (un nombre entier positif): ");
    scanf("%d", &key);

    encrypt(message, key);

    printf("Message chiffré: %s", message);

    return 0;
}
